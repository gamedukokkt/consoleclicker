﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleClicker
{
    class Program
    {        
        static void Main(string[] args)
        {
            Game clickerGame = new Game();
            clickerGame.Run();
        }
    }
}
