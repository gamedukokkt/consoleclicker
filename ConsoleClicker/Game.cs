﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleClicker
{
    class Game
    {
        ConsoleKey lastKey = ConsoleKey.NoName;
        string product = "MegaForce";
        int amount = 0;
        int productLimit = 200;
        double money = 0;

        int factories = 0;
        int factoryEfficiency = 1;
        double factoryPrice = 100.0;

        bool autoSellerEnabled = false;

        public void Run()
        {
            DrawInstructionsPanel();
            // Game loop
            while (lastKey != ConsoleKey.Q)
            {
                Update();
                Draw();
            }
        }
        void Update()
        {
            // Read player input
            lastKey = Console.ReadKey().Key;
            switch (lastKey)
            {
                case ConsoleKey.C:
                    AddProduct(1);
                    break;
                case ConsoleKey.S:
                    SellProduct(24, 0.90);
                    break;
                case ConsoleKey.F:
                    BuyFactory();
                    break;
                case ConsoleKey.A:
                    autoSellerEnabled = !autoSellerEnabled;
                    break;
                case ConsoleKey.U:
                    UpgradeProductLimit();
                    break;
                case ConsoleKey.P:
                    UpgradeFactories();
                    break;
            }

            // Factory operations
            for(int i = 0; i < factories; i++)
            {
                Manufacture();
            }
            // Auto selling
            if (autoSellerEnabled)
            {
                if(amount == productLimit)
                {
                    SellProduct(amount, 0.45);
                }
            }
        }

        void Draw()
        {
            Console.Clear();
            DrawMoneyPanel();
            DrawProductPanel();
            DrawAutoSellPanel();
            DrawInstructionsPanel();
            DrawFactoryPanel();
            /*Console.WriteLine("Amount of " + product + ": " + amount);
            Console.WriteLine("Factories: {0}, efficiency {1}, cost {2}", factories, factoryEfficiency, factoryPrice);
            if (autoSellerEnabled)
            {
                Console.WriteLine("AutoSell on.");
            }
            Console.WriteLine("[C]reate - [S]ell - [A]utoSell - Buy [F]actory - [U]pgrade product limit - U[p]grade factories");

            DrawTextBox("Money: " + money, 0, 0, 16, 5);*/
        }

        void WriteAt(int x, int y, string str)
        {
            Console.SetCursorPosition(x, y);
            Console.Write(str);
        }

        void DrawMoneyPanel()
        {
            int x = 2;
            int y = 2;
            DrawBox(x, y, 24, 5);
            WriteAt(x + 2, y + 2, "Money: " + money);
        }

        void DrawProductPanel()
        {
            int x = 27;
            int y = 8;
            DrawBox(x, y, 24, 6);
            WriteAt(x + 2, y + 2, "Product: " + product);
            WriteAt(x + 2, y + 3, "Storage: " + amount + "/" + productLimit);
        }
        void DrawAutoSellPanel()
        {
            int x = 27;
            int y = 2;
            DrawBox(x, y, 24, 5);
            WriteAt(x + 2, y + 2, "AutoSell: " + autoSellerEnabled);
        }

        void DrawInstructionsPanel()
        {
            int x = 2;
            int y = 8;
            DrawBox(x, y, 24, 14);
            WriteAt(x + 2, y + 2, "[C]reate");
            WriteAt(x + 2, y + 3, "[S]ell");
            WriteAt(x + 2, y + 4, "[A]utoSell");
            WriteAt(x + 2, y + 5, "Buy [F]actory");
            WriteAt(x + 2, y + 6, "[U]pgrade storage");
            WriteAt(x + 2, y + 7, "U[p]grade factories");
        }

        void DrawFactoryPanel()
        {
            int x = 27;
            int y = 15;
            DrawBox(x, y, 24, 7);
            WriteAt(x + 2, y + 2, "Factories: " + factories);
            WriteAt(x + 2, y + 3, "Efficiency: " + factoryEfficiency);
            WriteAt(x + 2, y + 3, "Upgrade price:" + factoryPrice);
        }
        void UpgradeFactories()
        {
            double upgradePrice = Math.Pow(250 * factoryEfficiency, 2);
            if(upgradePrice <= money)
            {
                money -= upgradePrice;
                factoryEfficiency++;
            }
        }

        void UpgradeProductLimit()
        {
            double upgradePrice = productLimit * 2;
            if (money >= upgradePrice)
            {
                money -= upgradePrice;
                productLimit *= 2; //productLimit = productLimit * 2;
            }
        }

        void BuyFactory()
        {
            if(money >= factoryPrice)
            {
                money -= factoryPrice;
                factories++;

                factoryPrice = 100.0 * (factories+1);
            }
        }

        void Manufacture()
        {
            AddProduct(factoryEfficiency);
        }

        void AddProduct(int amt)
        {
            amount += amt;
            if(amount > productLimit)
            {
                amount = productLimit;
            }
        }

        void SellProduct(int amt, double price)
        {
            int sellAmount = amt;
            if(sellAmount > amount)
            {
                sellAmount = amount;
            }
            amount -= sellAmount;
            money += sellAmount * price;
        }

        void DrawBox(int x, int y, int width, int height)
        {
            for(int i = 0; i < height; i++)
            {
                for(int j = 0; j < width; j++)
                {
                    Console.CursorTop = i + y;
                    Console.CursorLeft = j + x;
                    if(i == 0 || i == height-1 || j == 0 || j == width - 1)
                    {
                        Console.Write('#');
                    } else
                    {
                        Console.Write(' ');
                    }
                }
            }
        }

        void DrawTextBox(string txt, int x, int y, int width, int height)
        {
            DrawBox(x, y, width, height);
            Console.CursorTop = y + 2;
            Console.CursorLeft = x + 2;
            Console.Write(txt);

        }
    }
}
